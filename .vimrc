
colorscheme default
syntax enable
filetype plugin indent on

" fzf
set rtp+=~/.fzf

set modeline
set background=dark
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set smartindent

set number
set showcmd
set cursorline
set wildmenu
set showmatch
set incsearch

let g:airline_powerline_fonts = 1
let g:airline_theme = 'angr'
let g:airline#extensions#gutentags#enabled = 1
let g:gutentags_cache_dir = "~/.cache/gutentags"

let g:netrw_liststyle = 3
let g:netrw_banner = 0
let g:netrw_browse_split = 4
let g:netrw_winsize = 20

let g:rustfmt_autosave = 1

