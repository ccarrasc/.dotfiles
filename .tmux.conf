setw -g mode-keys vi
set -g mouse on

set -g base-index 1
set -g set-titles on
set -g set-titles-string "#{window_name}"

set -g default-shell /bin/zsh
set -g default-terminal "tmux"
set -g update-environment 'DISPLAY SSH_ASKPASS SSH_AGENT_PID SSH_CONNECTION WINDOWID XAUTHORITY TERM'
if "[[ ${TERM} =~ 256color || ${TERM} == fbterm ]]" 'set -g default-terminal tmux-256color'

# Panes
setw -g pane-base-index 1
set -g pane-border-style "fg=colour222"
set -g pane-active-border-style "fg=colour202"
#set -g window-style "fg=colour247"
#set -g window-active-style "fg=colour250,bg=black"
#set -g window-active-style "fg=colour250"
set -g window-size largest
setw -g aggressive-resize on

# Status bar
set -g display-time 0
set -g status-interval 5
set -g status-position top
set -g status-justify left
set -g status-left-style "fg=colour037"
set -g status-right-style "fg=colour037"
set -g status-style "bg=colour234"
set -g message-style "bg=colour202,fg=colour255"
setw -g window-status-style "fg=colour251"
setw -g window-status-current-style "bg=colour030,fg=colour251"

# Use - and | for pane splitting
# and use current path for new windows and panes
unbind '"'
unbind %
bind | split-window -h -c '#{pane_current_path}'
bind - split-window -v -c '#{pane_current_path}'
bind c neww -c '#{pane_current_path}'

# Copy mouse selection to clipboard
if-shell -b "command -v reattach-to-user-namespace > /dev/null 2>&1" {
    bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel 'reattach-to-user-namespace pbcopy' 
    bind-key -T copy-mode MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel 'reattach-to-user-namespace pbcopy'
}
# xsel on linux
if-shell -b "command -v xsel /dev/null 2>&1" {
    bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel 'xsel -i -p && xsel -o -p | xsel -i -b'
    bind-key -T copy-mode MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel 'xsel -i -p && xsel -o -p | xsel -i -b'
    bind-key -T copy-mode-vi y send-keys -X copy-pipe-and-cancel 'xsel -i -p && xsel -o -p | xsel -i -b'
    bind-key p run 'xsel -o | tmux load-buffer - ; tmux paste-buffer'
}
# wsl
if-shell -b "command -v clip.exe /dev/null 2>&1" {
    bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel clip.exe
    bind-key -T copy-mode MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel clip.exe
    bind-key -T copy-mode-vi y send-keys -X copy-pipe-and-cancel clip.exe
}

# Reload
bind r source-file ~/.tmux.conf

