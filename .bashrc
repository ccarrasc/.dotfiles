source $HOME/.aliases

# https://git-scm.com/book/id/v2/Appendix-A%3A-Git-in-Other-Environments-Git-in-Bash
. ~/git-completion.bash
. ~/git-prompt.sh
export GIT_PS1_SHOWDIRTYSTATE=1
export PS1='\w$(__git_ps1 " (%s)")\$ '

. /usr/share/bash-completion/completions/pass

#export ANDROID_HOME=~/Library/Android/sdk
#export ANDROID_NDK=~/Library/Android/ndk/21d
export LINARO_BIN=~/gcc-linaro-4.9-2016.02-x86_64_arm-linux-gnueabihf/bin

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
. ~/npm-completion.sh

# https://github.com/junegunn/fzf
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# Add rust's cargo to PATH
export PATH="$HOME/.cargo/bin:$PATH"

# rustup help completions
. ~/.local/share/bash-completion/completions/rustup
. ~/.local/share/bash-completion/completions/cargo

