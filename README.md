Reference: https://wiki.archlinux.org/index.php/Dotfiles#Tracking_dotfiles_directly_with_Git

Installation
```bash
git clone --bare --recurse-submodules git@gitlab.com:ccarrasc/.dotfiles.git $HOME/.dotfiles
alias config="$(which git) --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
config checkout
config config status.showUntrackedFiles no
$HOME/config-init.sh
```

Be sure to update your global gitconfig since mine is checked in :D

Add a dotfile
```bash
config add .foo
```

Some vim plugins will require installation of Univarsal Ctags: https://ctags.io/
```bash
pacman -S ctags powerline-fonts
```

To use in WSL2, open Powershell "As an Administrator" and run the following:
```ps1
git clone https://github.com/powerline/fonts.git
Set-ExecutionPolicy Bypass
cd .\fonts\
.\install.ps1
Set-ExecutionPolicy Default
```

Then in the WSL2, install powerline fonts, e.g., `sudo apt install fonts-powerline`, and set the terminal "Properties" (and Default) to use a Powerline font.

