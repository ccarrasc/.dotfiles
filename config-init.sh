#! /usr/bin/zsh

# fetch git completion scripts
if test ! -f $HOME/git-completion.bash; then
    wget -N -P $HOME/ https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash
fi

if test ! -f $HOME/git-prompt.sh; then
    wget -N  -P $HOME/ https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh
fi

mkdir -p $HOME/.zsh

if test ! -f $HOME/.zsh/git-completion.zsh; then
    wget -N -P $HOME/.zsh/ https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.zsh
fi

if command -v pass && test ! -f $HOME/.zsh/pass.zsh.completion; then
    wget -N -P $HOME/.zsh/ https://git.zx2c4.com/password-store/plain/src/completion/pass.zsh-completion 
fi

if ! command -v fzf; then
    ~/.fzf/install
else
    echo "fzf is already installed"
fi

if ! command -v nvm; then
    # install nvm
    wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash
    npm completion >> ~/npm-completion.sh
else
    echo "nvm is already installed"
fi

if command -v rustup; then
    mkdir -p ~/.local/share/bash-completion/completions
    rustup completions bash > ~/.local/share/bash-completion/completions/rustup
    rustup completions bash cargo > ~/.local/share/bash-completion/completions/cargo
    rustup completions zsh > ~/.zsh/_rustup
    rustup completions zsh cargo > ~/.zsh/_cargo
else
    echo "rustup is not installed. Will not add completions."
fi

# Add vim plugin help docs
for d in ~/.vim/pack/plugins/start/*/ ; do
    vim -u NONE -c "helptags $d/doc" -c q
done
