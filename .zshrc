#zmodload zsh/zprof

# --- History ---------------------------------------------------------------------
EXTENDED_HISTORY=1
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.history

# --- Environment -----------------------------------------------------------------
# Android/LineageOS
export ANDROID_HOME=/data/Android/Sdk
if [ -d "$ANDROID_HOME/platform-tools" ] ; then
    PATH="$ANDROID_HOME/platform-tools:$PATH"
fi

# ccache (probably don't need for every shell, but I use it)
export USE_CCACHE=1
export CCACHE_EXEC=/usr/bin/ccache
export CCACHE_DIR=/data/ccache
ccache --max-size 50G
ccache --set-config=sloppiness=locale,time_macros
PATH="/usr/lib/ccache/bin:$PATH"

export LINARO_BIN=~/gcc-linaro-4.9-2016.02-x86_64_arm-linux-gnueabihf/bin
export NVM_DIR="$HOME/.nvm"
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$PATH:$HOME/.rvm/bin"

# --- Completions -----------------------------------------------------------------
fpath+=~/.zsh

# Enable completion for pass: https://www.passwordstore.org/
fpath=(~/.zsh ~/.zsh/pass.zsh-completion $fpath)

# https://github.com/gradle/gradle-completion/blob/master/README.md#install-manually
fpath=(~/.zsh ~/.zsh/gradle-completion $fpath)

autoload -Uz compinit
compinit

zstyle ':completion:*' menu select

# nvm
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
. ~/npm-completion.sh

# https://github.com/junegunn/fzf
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# --- Git Prompt ------------------------------------------------------------------
# https://git-scm.com/book/id/v2/Appendix-A%3A-Git-in-Other-Environments-Git-in-Zsh
setopt prompt_subst
. ~/git-prompt.sh
export RPROMPT='[%F{yellow}%?%f]'
precmd () { __git_ps1 "%F{red}%n%f@%F{red}%m%f: %B%~%b" "%s%% " }
export GIT_PS1_SHOWDIRTYSTATE=1
export GIT_PS1_SHOWCOLORHINTS=1

# --- Aliases ---------------------------------------------------------------------
. ~/.aliases

